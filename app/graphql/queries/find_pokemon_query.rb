module Queries
    class FindPokemonQuery < GraphQL::Schema::Resolver
      argument :id, ID, required: true
  
      type Types::PokemonType, null: true
  
      def resolve(id:)
        Pokemon.find_by(id: id)
      end
    end
  end
  