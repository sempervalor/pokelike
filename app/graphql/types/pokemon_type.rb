module Types
    class PokemonType < GraphQL::Schema::Object
      # Define the fields for the PokemonType object
    field :id, ID, null: false
    field :name, String, null: false
    field :Pokemon, String, null: false
      # Add other fields as needed
    
      # Optional: Add any resolver methods or associations here
    
      # Optional: Define any connections or edge types here
    
      # Optional: Define any input object types here
    
      # Optional: Define any interfaces this type implements here
    end
  end
