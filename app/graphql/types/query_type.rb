module Types
  class QueryType < Types::BaseObject
    field :find_pokemon, resolver: Queries::FindPokemonQuery
  end

end

