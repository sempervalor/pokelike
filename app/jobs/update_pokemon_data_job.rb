class UpdatePokemonDataJob < ApplicationJob
    queue_as :default
  
    def perform
      # Fetch and update Pokemon data from PokeAPI
      Pokemon.update_pokemon_data
    end
  end
  