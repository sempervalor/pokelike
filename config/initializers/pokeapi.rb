module PokeApi
    class << self
      attr_accessor :configuration
    end
  
    def self.configure
      self.configuration ||= Configuration.new
      yield(configuration)
    end
  
    class Configuration
      attr_accessor :api_key, :base_uri
  
      def initialize
        @api_key = nil
        @base_uri = 'https://pokeapi.co/api/v2/'
      end
    end
  end
  
  PokeApi.configure do |config|
    # You can set additional configuration options here if needed
    config.api_key = 'YOUR_API_KEY'
  end
  