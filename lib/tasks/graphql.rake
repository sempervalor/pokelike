require 'json'

# Rake tasks for dumping the GraphQL schema.
# Used by ESLint and by the IDE for highlighting and autocomplete.
# Must be run on each schema change.
# The resulting files have to be included in version control for easier code reviews.
namespace :graphql do
  namespace :dump do
    desc 'Dumps the IDL schema into .graphql.schema.graphql'
    task idl: [:environment] do
      target = Rails.root.join('graphql.schema.graphql')
      schema = GraphQL::Schema::Printer.print_schema(PokeApiAppSchema)
      File.open(target, 'w+') do |f|
        f.write(schema)
      end
      puts 'Schema dumped into graphql.schema.graphql'
    end

    desc 'Dumps the result of the introspection query into ./graphql.schema.json'
    task json: [:environment] do
      target = Rails.root.join('graphql.schema.json')
      result = PokeApiAppSchema.execute(GraphQL::Introspection::INTROSPECTION_QUERY)
      File.open(target, 'w+') do |f|
        f.write(JSON.pretty_generate(result))
      end
      puts 'Schema dumped into graphql.schema.json'
    end

    # Reference: https://www.apollographql.com/docs/react/v2/data/fragments/
    desc 'Dumps the result of fragment types for IntrospectionFragmentMatcher into ./fragmentTypes.json'
    task fragment: [:environment] do
      target = Rails.root.join('fragmentTypes.json')

      result_schema = PokeApiAppSchema.execute(GraphQL::Introspection::INTROSPECTION_QUERY)
                                        
      # here we're filtering out any type information unrelated to unions or interfaces types
      result_schema['__schema'].slice!('types')
      result_schema['__schema']['types'].select! { |t| t['possibleTypes'].present? }
      File.open(target, 'w+') do |f|
        f.write(JSON.pretty_generate(result_schema))
      end
      puts 'Fragment schema dumped into fragmentTypes.json'
    end
  end

  desc 'Dumps the IDL, the schema json, and the schema for fragment types in root'
  task dump: %w[graphql:dump:idl graphql:dump:json graphql:dump:fragment]
end
